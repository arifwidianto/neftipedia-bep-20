# NEFTiPEDiA $NFT Token

Applied as BEP-20 (ERC-20)

## Short Notes

This just a **backup copy** of Smart Contracts from deployed at the BSC MainNet.

We will fix and makeover with more info, documentation and coverage test.

**Have not been Audited yet!**. Current  progress has been request to Certik.org as the common Auditor. Hopefully they will proceed and release soon enough with good results.
